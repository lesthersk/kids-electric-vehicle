# This file was created solely for the purpose of calculating the values
# of the most common resistors in a csv file
# tolerance_group_1 includes tolerances of 0.1, 0.25, 0.5%
# tolerance_group_2 includes tolerances of 1%
# tolerance_group_3 includes tolerances of 2, 5%
# tolerance_group_4 includes tolerances of 10%
# based off of Standard Electronic Decade Value Tables from vishay

import pandas

input_file = 'tabula_dectable.csv'
parsed_resistor_values = pandas.read_csv(input_file)
parsed_resistor_groups = list(parsed_resistor_values)

parsed_resistor_data = {

}

def get_all_resistor_values():
    global parsed_resistor_data
    # Iterate over the available columns
    for column_index in range(len(parsed_resistor_groups)):
        # iterate over the length of each column (possibly does not include NAN values)
        row_data = []

        for row_index in range(int(parsed_resistor_values[parsed_resistor_groups[column_index]].count())):
            row_data.append(parsed_resistor_values.loc[row_index, parsed_resistor_groups[column_index]])

        for row_index in range(0, int(parsed_resistor_values[parsed_resistor_groups[column_index]].count())*5):
            row_data.append(row_data[row_index]*10)

        # insert the calculated values into the dictionary
        parsed_resistor_data.update({parsed_resistor_groups[column_index]: row_data})

    return parsed_resistor_data