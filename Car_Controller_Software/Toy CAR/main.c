/*
 * Toy CAR.c
 *
 * Created: 3/19/2021 10:37:10 AM
 * Author : Lesther Borge
 */ 
#define F_CPU 1000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include "IO_Macros.h"

/*
Things to keep in mind when selecting input pins

PCI0 will trigger if any enabled PCINT[7:0] pin toggles
PCI1 will trigger if any enabled PCINT[14:8] pin toggles
PCI2 will trigger if any enabled PCINT[23:16] pin toggles
*/
#define pedal_pin               B,      PINB6   //input > acceleration pedal  - PCINT6 - PCI0
#define direction_pin           B,      PINB7   //input > direction stick     - PCINT7 - PCI0
#define turn_on_car_pin         D,      PIND5   //input > turn on-off car pin - PCINT21 

/*
TODO, configuration on how the motor moves forward and backwards
Motor direction 1 goes to pin I1 - pin12 - 'D'
Motor direction 2 goes to pin I2 - pin10 - 'C'
Bring motor_speed to zero to free run motor stop (motor stops on its own)

Ven = High 
C = High    ; D = Low    Forward
C = Low     ; D = High   Reverse

Ven = L
C = X ; D = X   Free Running Motor Stop
X - dont care 

*/
#define motor_direction_1   D,      PIND6   //output - D
#define motor_direction_2   D,      PIND7   //output - C
#define motor_control       B,      PINB1   //output - enable-disable motor driver

#define go_forward      0x07
#define go_backwards    0x70
volatile int8_t direction_position = 0;

#define pedal_pressed   0x04
#define pedal_released  0x40
volatile int8_t pedal_position = 0;

void move_backwards();
void move_forward();
void accelerate_car();
void stop_car();

#define intShorcut 1


ISR(PCINT0_vect)
{
    
    //Check if the kiddo wants to change directions
    if (DigitalRead(direction_pin)) //If yes
        #if intShorcut
            move_forward();
        #else
            direction_position = go_forward;
        #endif
        
    else //If not
        #if intShorcut
            move_backwards();
        #else
            direction_position = go_backwards;
        #endif
            
        
        
    //Check if the pedal has been pressed
    if (DigitalRead(pedal_pin)) //pedal pressed brings pin to logic high
        #if intShorcut
            stop_car();
        #else
            pedal_position = pedal_released;
        #endif
    else //pedal pin is now logic low, pedal is pressed
        #if intShorcut
            accelerate_car();
        #else
            pedal_position = pedal_pressed;
        #endif
    
}

int main(void)
{
    #if intShorcut == 0
        volatile int8_t direction = 0;
        volatile int8_t pedal = 0;
    #endif

    volatile uint8_t isCarOn = 0;

    /*Deactivating interrupts*/
    cli(); 
    /*Setting pin functionality*/
    PinMode(pedal_pin, Input);
    PinMode(direction_pin, Input);
    PinMode(turn_on_car_pin, Input);
    PinMode(motor_direction_1, Output);
    PinMode(motor_direction_2, Output);
    PinMode(motor_control, Output);
    
    /*Setting up interrupt functionality*/
    PCICR  |= (1 << PCIE0);
    PCMSK0 |= (1 << PCINT7) | (1 << PCINT6);
    
    /*Setting up the pwm mode for the motor -- LEAVE COMMENTED, FUTURE RELEASE*/
    //TCCR1A |= (1 << COM1A1) | (1 << COM1A0);//Fast PWM, 8-bit
    //TCCR1A |= (1 << WGM10);
    //TCCR1B |= (1 << WGM12);
    //TCCR1B |= (1 << CS11); //divider of 8 freq of 3.9khz
    //OCR1A = 0;
    
    /*Enabling interrupts*/
    sei();

    stop_car();

    while (1) 
    {
        /*Check of the enable switch is on*/
        isCarOn = DigitalRead(turn_on_car_pin);
        
        if (isCarOn)
        {
            #if intShorcut == 0
            
                if (direction != direction_position || pedal != pedal_position)
                {
                    
                    
                    /*Detect when kiddo wants to reverse the car */
                    switch(direction_position)
                    {

                        case go_forward:
                        move_forward();
                        break;

                        case go_backwards:
                        move_backwards();
                        break;
                    }

                    direction = direction_position;
                    
                    /*Detect if kiddo wants to brake or accelerate*/
                    switch(pedal_position)
                    {
                        
                        case pedal_pressed:
                        accelerate_car();
                        break;
                        
                        case pedal_released:
                        stop_car();
                        break;
                        
                    }
                    
                    pedal = pedal_position;
                }

            #endif

           
        }
        else /*if the carn switch is in the off position, we stop the car*/
        {
            
            stop_car();
            
        }            
            
    }

}


void move_forward()
{

    stop_car();
    DigitalWrite(motor_direction_2, High);
    DigitalWrite(motor_direction_1, Low);

}

void move_backwards()
{
    stop_car();
    DigitalWrite(motor_direction_1,  High);
    DigitalWrite(motor_direction_2, Low);

}


void accelerate_car()
{
    #if intShorcut == 0
        _delay_ms(200);
    #endif
    
    DigitalWrite(motor_control,  High);

}

void stop_car()
{

    DigitalWrite(motor_control,  Low);
    DigitalWrite(motor_direction_1, Low);
    DigitalWrite(motor_direction_2, Low);

}
 

