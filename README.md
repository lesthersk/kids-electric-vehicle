# Electric Car Toy Controller

## Description
This project was built so there exist an easy to build DC motor controller for Electric Toy Cars such as this one:

## Background
I tasked myself recently to repair a small Electric Car for kids, someone very close to me had one laying around that just stopped working and asked me for help to give it some extra life; so the first thing I did was look online for readily available solutions that I could just plug and play with the car, but I became frustrated, the ones available such as [this one](https://hobbyking.com/en_us/turnigy-30a-brushed-esc.html) did not include great deal of documentation or brushed motors interface.

Natually I decided to rull out my own, this cars tend to include a handful of buttons, be it for a horn, turn on-off front or back lights, etc. I tried to keep it as simple as possible for the kiddo driving it so I included:

* On off switch.
* Acceleration pedal.
* Forward and reverse switch.

## Results
TODO

## Tools and materials
This project was built using the following tools and materials:

- The schematic was designed in [Altium Designer 22](https://www.altium.com/products/downloads), I believe this should be backwards compatible with previous Altium versions, but I'm yet to try it.
- Microcontroller code was developed in [Microchip Studio (formerly Atmel Studio)](https://www.microchip.com/en-us/development-tools-tools-and-software/microchip-studio-for-avr-and-sam-devices) using the AVR GCC Toolchain, not the MPLAB XC8 one; the microcontroller I used was a bare ATmega328P in it's DIP 28 package.
- 2 [LN298] (https://www.st.com/en/motor-drivers/l298.html) mounted in a ready to use development board, this should run about ~$9 each, depending on where you get them.
- A perfboard of about 10x8 CM.
- A handful miscellaneous components (resistors, ceramic capacitors, headers)

# Usage
- Open the Microchip Studio project and do a clean build to generate the hex file according to your AVR controller of choice.
- Flash the microcontroller code using your favorite programmer, I used an [Atmel ICE](https://www.microchip.com/en-us/development-tool/ATATMEL-ICE).
- You can create a pcb file in Altium to route a board to have it manufactured by your favorite fab house, they are quite affordable nowadays, or you may mount the components in a perfboard as I did
- Place the components according to provided [schematic](https://gitlab.com/lesthersk/kids-electric-vehicle/-/blob/main/Car_Controller_Hardware/main_schematic.SchDoc).

## Roadmap
As of now, I consider this project is complete, but I will be adding some battery charging functionality on a future version.

## Authors and acknowledgment
Big kuddos to folks at the AVR Freaks forum for sharing a lot of their knowledge for free.

## License
This project is released under GNU General Public License v3.0.

## Project status
In Progress
